# Проецирование порта 5000 на встроенный в minukube registrer
# -d - запуск в фоне, -it запуск в интерактиве

minikube addons enable registry
docker run -d  --name minikube_register_tun --restart=always --network=host alpine ash -c "apk add socat && socat TCP-LISTEN:5000,reuseaddr,fork TCP:$(minikube ip):5000"
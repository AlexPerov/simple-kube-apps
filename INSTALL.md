# Программные пакеты 

## docker - Базовое приложение для контейнеризации.

[Документация по установке](https://docs.docker.com/engine/install/)

для ubuntu - https://docs.docker.com/engine/install/ubuntu/


## docker-composer - Запуск многоконтейнерного приложения для хосту

установка https://docs.docker.com/compose/install/ 


## kubectl 

утилита управления кластером кубернетикс

https://kubernetes.io/docs/tasks/tools/install-kubectl/

## minikube - Кластер кубернетис на локальном комьютере

https://kubernetes.io/docs/tasks/tools/install-kubectl/

## helm - Пакетный менеджер для kubernetes

https://helm.sh/docs/intro/install/

## kubectx - утилита переключения контекстов

```
brew install kubectx
```

## kubens - переключение namespace

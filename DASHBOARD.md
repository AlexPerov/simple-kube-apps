# Панель мониторинга и управления кластером

Почитать здесь:
https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/


Запускаем прокси
```
kubectl proxy --port 8200
```

Потом обращаемся на 
lynx http://localhost:8200/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/.

Обратите внимание на точку в конце адреса - она нужна и важна.


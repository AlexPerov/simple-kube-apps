# HELM - пакетный менеджер для kubernetes

примеры для версии 3.

## Установка 

```
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
```

## Создание чарта

```bash
helm create app-1-nodejs-pg
```

## Инсталяция чарта

* --dry-run  симмуляция инсталяция
* --debug дополнительный отладочный выход

Инсталяция программы из чарта

```bash
helm install app-1-nodejs-pg ./app-1-nodejs-pg --dry-run --debug
```

Получить список релизов в кластере

```bash
helm list
```

Удалить релиз

```bash
helm uninstall app-1-nodejs-pg
```
# Запуск кластера kubernetes в виртуалках по виртуалбокс

1. Поставить virtualbox
2. Поставить vagrant
3. Поставить kubectl

Запускаем ./000_create_k8s_cluster.sh

Если все идет хорошо, получаем 3 работающие вирутуалки
с настроенным кластером k8s и dashboard.

После запуска должна работать команда:

```
nedlosster@nedlosster-ws:~/project/simple-kube-apps/setup_simple_k8s_cluster$ kubectl get nodes -o wide
NAME   STATUS   ROLES    AGE     VERSION   INTERNAL-IP   EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION      CONTAINER-RUNTIME
kub1   Ready    master   3m23s   v1.18.3   10.0.2.15     <none>        Ubuntu 18.04.4 LTS   4.15.0-99-generic   docker://19.3.6
kub2   Ready    <none>   2m56s   v1.18.3   10.0.2.15     <none>        Ubuntu 18.04.4 LTS   4.15.0-99-generic   docker://19.3.6
kub3   Ready    <none>   2m47s   v1.18.3   10.0.2.15     <none>        Ubuntu 18.04.4 LTS   4.15.0-99-generic   docker://19.3.6

```

Внимательно смотрим вывод - и заходим в консоль.

Изучаем и пробуем.

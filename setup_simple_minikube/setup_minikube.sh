sudo apt-get clean && sudo apt-get update && sudo apt-get install -y apt-transport-https curl
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
cat <<EOF | sudo tee /etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF
sudo apt-get update
sudo apt-get install -y docker.io kubectl
sudo apt-mark hold docker.io kubectl

sudo usermod -aG docker vagrant

sudo systemctl daemon-reload

curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb
sudo dpkg -i minikube_latest_amd64.deb

#sudo echo "source <(kubectl completion bash)" >> /etc/bash.bashrc 

cat << EOF | sudo tee /etc/bash.bashrc
source <(kubectl completion bash)
alias k=kubectl
complete -F __start_kubectl k
EOF
